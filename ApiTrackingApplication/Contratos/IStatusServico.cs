﻿using ApiTrackingDominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiTrackingApplication.Contratos
{
    public interface IStatusServico
    {
        Task<List<Status>> GetStatus();

        /*
         * Entidade
         */
        Task<Status> GetStatusEntidadeCliente(string orderId);
        Task<Status> GetStatusEntidadeSAP(string orderId);
        Task<Status> GetStatusEntidadeSalesForce(string orderId);

        /*
         * Header
         */
        Task<Status> GetStatusHeaderCliente(string orderId);
        Task<Status> GetStatusHeaderSAP(string orderId);
        Task<Status> GetStatusHeaderSalesForce(string orderId);

        /*
         * Itens
         */
        Task<Status> GetStatusItensCliente(string orderId);
        Task<Status> GetItemCliente(string orderId, string itemId);
        Task<Status> GetStatusItemCliente(string orderId, string itemId);

        Task<Status> GetStatusItensSAP(string orderId);
        Task<Status> GetItemSAP(string orderId, string itemId);
        Task<Status> GetStatusItemSAP(string orderId, string itemId);

        Task<Status> GetStatusItensSalesForce(string orderId);
        Task<Status> GetItemSalesForce(string orderId, string itemId);
        Task<Status> GetStatusItemSalesForce(string orderId, string itemId);


        /*
         * Insert Fila / MONGODB
         */
        Task<Status> SetStatusMongoFila(Status status);

        /*
         * 
         */
        Task<Status> GetStatusFila();
        
    }
}

﻿using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using ApiTrackingApplication.Contratos;
using ApiTrackingDominio.Entidades;
using ApiTrackingRepositorio.Contratos;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiTrackingApplication.ApiTrackingServicos
{
    public class StatusServico : IStatusServico
    {
        private readonly IAmazonSQS sqs = new AmazonSQSClient(RegionEndpoint.USEast2);
        private readonly string filaNome = "tracking";

        private readonly IStatusRepositorio _statusRepositorio;
        
        public StatusServico(IStatusRepositorio statusRepositorio)
        {
            _statusRepositorio = statusRepositorio;
        }


        private string GetQueueUrl()
        {
            return sqs.GetQueueUrlAsync(filaNome).Result.QueueUrl;
        }

        public async Task<List<Status>> GetStatus()
        {
            var lstStatus = await _statusRepositorio.BuscarListaStatus();

            return lstStatus;

        }


        #region Entidade

        public async Task<Status> GetStatusEntidadeCliente(string orderId)
        {
            var status = await _statusRepositorio.BuscarEntidadeCliente(orderId);

            return status;
        }

        public async Task<Status> GetStatusEntidadeSAP(string orderId)
        {
            var status = await _statusRepositorio.BuscarEntidadeSAP(orderId);

            return status;
        }

        public async Task<Status> GetStatusEntidadeSalesForce(string orderId)
        {
            var status = await _statusRepositorio.BuscarEntidadeSalesForce(orderId);

            return status;
        }
        #endregion

        #region Header
        public async Task<Status> GetStatusHeaderCliente(string orderId)
        {
            var status = await _statusRepositorio.BuscarHeaderCliente(orderId);

            return status;
        }

        public async Task<Status> GetStatusHeaderSAP(string orderId)
        {
            var status = await _statusRepositorio.BuscarHeaderSAP(orderId);

            return status;
        }

        public async Task<Status> GetStatusHeaderSalesForce(string orderId)
        {
            var status = await _statusRepositorio.BuscarHeaderSalesForce(orderId);

            return status;
        }
        #endregion

        
        #region Itens Cliente
        public async Task<Status> GetStatusItensCliente(string orderId)
        {
            var status = await _statusRepositorio.BuscarStatusItensCliente(orderId);

            return status;
        }
        public async Task<Status> GetItemCliente(string orderId, string itemId)
        {
            var item = await _statusRepositorio.BuscarItemCliente(orderId, itemId);

            return item;
        }
        public async Task<Status> GetStatusItemCliente(string orderId, string itemId)
        {
            var itemStatus = await _statusRepositorio.BuscarStatusItemCliente(orderId, itemId);

            return itemStatus;
        }
        #endregion

        #region Itesn SAP
        public async Task<Status> GetStatusItensSAP(string orderId)
        {
            var status = await _statusRepositorio.BuscarStatusItensSAP(orderId);

            return status;
        }
        public async Task<Status> GetItemSAP(string orderId, string itemId)
        {
            var item = await _statusRepositorio.BuscarItemSAP(orderId, itemId);

            return item;
        }
        public async Task<Status> GetStatusItemSAP(string orderId, string itemId)
        {
            var itemStatus = await _statusRepositorio.BuscarStatusItemSAP(orderId, itemId);

            return itemStatus;
        }
        #endregion

        #region Itens SalesForce
        public async Task<Status> GetStatusItensSalesForce(string orderId)
        {
            var status = await _statusRepositorio.BuscarStatusItensSalesForce(orderId, itemId);

            return status;
        }
        public async Task<Status> GetItemSalesForce(string orderId, string itemId)
        {
            var status = await _statusRepositorio.BuscarStatusItensSalesForce(orderId, itemId);

            return status;
        }
        public async Task<Status> GetStatusItemSalesForce(string orderId, string itemId)
        {
            var status = await _statusRepositorio.BuscarStatusItensSalesForce(orderId, itemId);

            return status;
        }
        #endregion




        public async Task<Status> SetStatusMongoFila(Status status)
        {

            Status st = new Status();

            //To do Lógica para agregar o objeto
            /*
             * 
             */

            /*
             * Inserir na Fila 
             */
                

            SendMessageRequest sendMessage = new SendMessageRequest();
            sendMessage.QueueUrl = GetQueueUrl();
            sendMessage.MessageBody = JsonConvert.SerializeObject(status);

            try
            {
                //sqs.SendMessageAsync(sendMessage);

                //if (VerificaSeExiste(status))
                //{
                //    AlterStatusMongo(status);
                //}
                //else
                //{
                    await SetStatusMongo(status);
                //}

                
                return status;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - erro ao inserir na fila");

                throw detalheExcecao;
            }
         
        }

        /*
         * Recupera informação da fila RabbitMQ
         */
        public async Task<Status> GetStatusFila()
        {
            
            Status st = new Status();

            string queueUrl = GetQueueUrl();

            var receiveMessageRequest = new ReceiveMessageRequest
            {
                QueueUrl = queueUrl
            };

            Task<ReceiveMessageResponse> receiveMessageResponse = sqs.ReceiveMessageAsync(receiveMessageRequest);
            
            foreach (Message message in receiveMessageResponse.Result.Messages)
            {
                st = JsonConvert.DeserializeObject<Status>(message.Body);
            }

            return st;
        }
         

        /*
         * Grava json no MONGODB
         */
        private async Task<Status> SetStatusMongo(Status status)
        {
            await _statusRepositorio.InsertStatusMongo(status);
            return status;
        }

        private async Task<bool> VerificaSeExiste(Status status)
        {
            return await _statusRepositorio.BuscaStatusExistente(status);
        }
        
    }
}

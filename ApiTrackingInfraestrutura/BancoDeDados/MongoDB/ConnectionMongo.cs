﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using MongoDB.Driver;
using ApiTrackingDominio.Entidades;
using ApiTrackingInfraestrutura.BancoDeDados.Contratos;

namespace ApiTrackingInfraestrutura.BancoDeDados.MongoDB
{
    public class ConnectionMongo : IConnection
    {
        public string StatusCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}

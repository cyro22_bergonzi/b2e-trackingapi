﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ApiTrackingInfraestrutura.BancoDeDados.Contratos
{
    public interface IConnection
    {
        string StatusCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}

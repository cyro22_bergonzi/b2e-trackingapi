﻿using ApiTrackingApplication.ApiTrackingServicos;
using ApiTrackingApplication.Contratos;
using ApiTrackingInfraestrutura.BancoDeDados.Contratos;
using ApiTrackingInfraestrutura.BancoDeDados.MongoDB;
using ApiTrackingRepositorio.Contratos;
using ApiTrackingRepositorio.Repositorio;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ApiTracking
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddTransient<IStatusServico, StatusServico>();
            services.AddTransient<IStatusRepositorio, StatusRepositorio>();

            services.Configure<ConnectionMongo>(Configuration.GetSection(nameof(ConnectionMongo)));
            services.AddSingleton<IConnection>(x => x.GetRequiredService<IOptions<ConnectionMongo>>().Value);

            services.AddSingleton<StatusServico>();
            services.AddSingleton<StatusRepositorio>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ApiTrackingApplication.Contratos;
using ApiTrackingDominio.Entidades;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiTracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private readonly ILogger<StatusController> _logger;
        private readonly IStatusServico _statusServico;

        public StatusController(ILogger<StatusController> logger, IStatusServico statusServico)
        {
            _logger = logger;
            _statusServico = statusServico;
        }


        // GET api/status
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Status>>> Get()
        {
            try
            {
                var status = await _statusServico.GetStatus();
                return Ok(status);
                
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da base de dados", ex);
                return new StatusCodeResult(404);
            }
            
        }


        #region Entidade
        /*
         * 
         * GET ENTIDADE
         * 
         */


        // GET Retorna Entidade com todos os status pelo número do Pedido do cliente
        [HttpGet("{id}")]
        [Route("sales/order/{orderId}")]
        public async Task<ActionResult<Status>> GetOrderCliente(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusEntidadeCliente(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir status da base de dados", ex);

                return new StatusCodeResult(404);
            }

        }


        // GET Retorna Entidade com todos os status pelo número do Pedido SAP
        [HttpGet("{id}")]
        [Route("sales/order/sap/{orderId}")]
        public async Task<ActionResult<Status>> GetOrderSAP(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusEntidadeSAP(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }


        // GET Retorna Entidade com todos os status pelo número do Pedido SALESFORCE
        [HttpGet("{id}")]
        [Route("sales/order/salesforce/{orderId}")]
        public async Task<ActionResult<Status>> GetOrderSalesForce(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusEntidadeSalesForce(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }
        #endregion

        #region Header
        /*
         * 
         * GET HEADER
         * 
         */

        // GET Retorna o último status do cabeçalho do pedido cliente
        [HttpGet("{id}")]
        [Route("sales/order/{orderId}/header/status")]
        public async Task<ActionResult<Status>> GetHeaderCliente(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusHeaderCliente(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o último status do cabeçalho do pedido SAP
        [HttpGet("{id}")]
        [Route("sales/order/sap/{orderId}/header/status")]
        public async Task<ActionResult<Status>> GetHeaderSAP(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusHeaderSAP(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o último status do cabeçalho do pedido SalesForce
        [HttpGet("{id}")]
        [Route("sales/order/salesforce/{orderId}/header/status")]
        public async Task<ActionResult<Status>> GetHeaderSalesForce(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusHeaderSalesForce(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }
        #endregion


        /*
         * 
         * GET ITENS
         * 
         */

        
        #region Itens Cliente
        // GET Retorna todos os itens e seus status
        [HttpGet()]
        [Route("sales/order/{orderId}/itens")]
        public async Task<ActionResult<Status>> GetItensCliente(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusItensCliente(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir os itens e seus status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o item informado
        [HttpGet()]
        [Route("sales/order/{orderId}/itens/{itemId}")]
        public async Task<ActionResult<Status>> GetItemCliente(string orderId, string itemId)
        {
            try
            {
                var item = await _statusServico.GetItemCliente(orderId, itemId);
                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir o item da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o status do item informado
        [HttpGet("{id}")]
        [Route("sales/order/{orderId}/itens/{itemId}/status")]
        public async Task<ActionResult<Status>> GetStatusItemCliente(string orderId, string itemId)
        {
            try
            {
                var status = await _statusServico.GetStatusItemCliente(orderId, itemId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir o status do item da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }
        #endregion




        #region Itens SAP
        // GET Retorna todos os itens e seus status SAP
        [HttpGet()]
        [Route("sales/order/sap/{orderId}/itens")]
        public async Task<ActionResult<Status>> GetItensCliente(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusItensCliente(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir os itens e seus status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o item informado SAP
        [HttpGet()]
        [Route("sales/order/sap/{orderId}/itens/{itemId}")]
        public async Task<ActionResult<Status>> GetItemSAP(string orderId, string itemId)
        {
            try
            {
                var item = await _statusServico.GetItemSAP(orderId, itemId);
                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir o item da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o status do item informado SAP
        [HttpGet("{id}")]
        [Route("sales/order/sap/{orderId}/itens/{itemId}/status")]
        public async Task<ActionResult<Status>> GetStatusItemSAP(string orderId, string itemId)
        {
            try
            {
                var status = await _statusServico.GetStatusItemSAP(orderId, itemId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir o status do item da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }
        #endregion




        #region Itens SalesForce
        // GET Retorna todos os itens e seus status SAP
        [HttpGet()]
        [Route("sales/order/sap/{orderId}/itens")]
        public async Task<ActionResult<Status>> GetItensCliente(string orderId)
        {
            try
            {
                var status = await _statusServico.GetStatusItensCliente(orderId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir os itens e seus status da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o item informado SAP
        [HttpGet()]
        [Route("sales/order/salesforce/{orderId}/itens/{itemId}")]
        public async Task<ActionResult<Status>> GetItemSaleForce(string orderId, string itemId)
        {
            try
            {
                var item = await _statusServico.GetItemSaleForce(orderId, itemId);
                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir o item da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }

        // GET Retorna o status do item informado SAP
        [HttpGet("{id}")]
        [Route("sales/order/salesforce/{orderId}/itens/{itemId}/status")]
        public async Task<ActionResult<Status>> GetStatusItemSalesForce(string orderId, string itemId)
        {
            try
            {
                var status = await _statusServico.GetStatusItemSaleForce(orderId, itemId);
                return Ok(status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir o status do item da base de dados", ex);
                return new StatusCodeResult(404);
            }

        }
        #endregion


        /*
         * 
         * POST - INSERT FILA E MONGODB
         * 
         */



        // POST Grava Entidade Status na Fila e no MONGODB
        [HttpPost]
        [Route("fila")]
        public async Task<IActionResult> InsertStatus([FromBody] Status status)
        {
            try
            {
                var retorno = await _statusServico.SetStatusMongoFila(status);
                return new StatusCodeResult(200);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar publicar na fila", ex);
                return new StatusCodeResult(500);
            }
            
        }

        // GET - Consome a Fila (RabbitMQ/SQS)
        [HttpGet]
        [Route("consume")]
        public async Task<IActionResult> ConsumeStatusFila()
        {
            try
            {
                var retorno = await _statusServico.GetStatusFila();
                return Ok(retorno);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir a fila", ex);
                return new StatusCodeResult(500);
            }
            
        }
        
    }
}

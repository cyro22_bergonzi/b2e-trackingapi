﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiTrackingDominio.Entidades
{
    public class Status
    {
        [BsonId]
        public Guid IndiceId { get; set; }

        [BsonElement]
        public string IndiceNome { get; set; }

        [BsonElement]
        public List<IndicesValores> IndiceValores { get; set; }

        [BsonElement]
        public decimal IndiceValorAcumulado { get; set; }

        [BsonElement]
        public DateTime IndiceData { get; set; }
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiTrackingDominio.Entidades
{
    public class IndicesValores
    {
        [BsonId]
        public Guid Id { get; set; }
        [BsonElement]
        public DateTime Data { get; set; }
        [BsonElement]
        public decimal Valor { get; set; }
        [BsonElement]
        public decimal ValorAcumulado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiTrackingApplication.ApiTrackingServicos;
using ApiTrackingDominio.Contratos;
using ApiTrackingDominio.Entidades;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiTrackingConsume.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private ILogger<ValuesController> _logger;
        private readonly IStatus _statusServico;

        public ValuesController(ILogger<ValuesController> logger, IStatus statusServico)
        {
            _logger = logger;
            _statusServico = statusServico;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Status>> Get()
        {
            try
            {
                var retorno = _statusServico.GetStatusMongo();
                return Ok(retorno);
            }
            catch (Exception ex)
            {
                _logger.LogError("Erro ao tentar consumir da fila", ex);
                return new StatusCodeResult(500);
            }
            
        }

    }
}

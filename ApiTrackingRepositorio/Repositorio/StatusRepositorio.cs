﻿using ApiTrackingDominio.Entidades;
using ApiTrackingInfraestrutura.BancoDeDados.Contratos;
using ApiTrackingInfraestrutura.BancoDeDados.SQLServer;
using ApiTrackingRepositorio.Contratos;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiTrackingRepositorio.Repositorio
{
    public class StatusRepositorio : IStatusRepositorio
    {
        private readonly ILogger<StatusRepositorio> _logger;
        private readonly IMongoCollection<Status> _mongoCollection;

        
        public StatusRepositorio(IConfiguration configuration, ILogger<StatusRepositorio> logger, IConnection connection)
        {
            _logger = logger;
            var client = new MongoClient(connection.ConnectionString);
            var database = client.GetDatabase(connection.DatabaseName);
            _mongoCollection = database.GetCollection<Status>(connection.StatusCollectionName);
        }

        public async Task<List<Status>> BuscarListaStatus()
        {
            try
            {
                return await _mongoCollection.Find(status => true).ToListAsync();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar a lista de status de todos os pedidos");

                throw detalheExcecao;

            }

        }


        #region Entidade

        public async Task<Status> BuscarEntidadeCliente(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderCliente == orderId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o pedido do cliente - {orderId}");

                throw detalheExcecao;

            }
        }

        public async Task<Status> BuscarEntidadeSAP(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSap == orderId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o pedido SAP - {orderId}");

                throw detalheExcecao;

            }

        }

        public async Task<Status> BuscarEntidadeSalesForce(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSalesForce == orderId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o pedido salesforce - {orderId}");

                throw detalheExcecao;

            }
        }
        #endregion

        #region Header
        public async Task<Status> BuscarHeaderCliente(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderCliente == orderId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o header do pedido cliente - {orderId}");

                throw detalheExcecao;

            }

        }

        public async Task<Status> BuscarHeaderSAP(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSap == orderId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o header do pedido SAP - {orderId}");

                throw detalheExcecao;

            }

        }

        public async Task<Status> BuscarHeaderSalesForce(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSalesForce == orderId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o header do pedido salesforce - {orderId}");

                throw detalheExcecao;

            }

        }
        #endregion


        #region Itens Cliente       
        public async Task<Status> BuscarStatusItensCliente(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderCliente == orderId);
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} cliente");

                throw detalheExcecao;

            }
        }
        public async Task<Status> BuscarItemCliente(string orderId, string itemId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderCliente == orderId && status.numItemClienteId == itemId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} cliente - {itemId}");

                throw detalheExcecao;

            }
        }
        public async Task<Status> BuscarStatusItemCliente(string orderId, string itemId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderCliente == orderId && status.numItemClienteId == itemId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} cliente - {itemId}");

                throw detalheExcecao;

            }
        }
        #endregion

        #region Itens SAP
        public async Task<Status> BuscarStatusItensSAP(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSAP == orderId);
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} SAP");

                throw detalheExcecao;

            }
        }
        public async Task<Status> BuscarItemSAP(string orderId, string itemId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSAP == orderId && status.numItemSAPId == itemId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} SAP - {itemId}");

                throw detalheExcecao;

            }
        }
        public async Task<Status> BuscarStatusItemSAP(string orderId, string itemId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSAP == orderId && status.numItemSAPId == itemId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} SAP - {itemId}");

                throw detalheExcecao;

            }
        }

        #endregion

        #region Itens SalesForce
        public async Task<Status> BuscarStatusItensSalesForce(string orderId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSalesForce == orderId);
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} salesforce");

                throw detalheExcecao;

            }
        }
        public async Task<Status> BuscarItemSalesForce(string orderId, string itemId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSalesForce == orderId && status.numItemSalesForceId == itemId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} salesforce - {itemId}");

                throw detalheExcecao;

            }
        }
        public async Task<Status> BuscarStatusItemSalesForce(string orderId, string itemId)
        {
            try
            {
                return await _mongoCollection.FindAsync(status => status.numOrderSalesForce == orderId && status.numItemSalesForceId == itemId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar os itens do pedido {orderId} salesforce - {itemId}");

                throw detalheExcecao;

            }
        }
        #endregion

        
        public async Task<Status> InsertStatusMongo(Status status)
        {
            try
            {
                await _mongoCollection.InsertOneAsync(status);
                return status;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao tentar inserir os dados - {status}");

                throw detalheExcecao;
            }

        }

        public async Task<bool> BuscaStatusExistente(Status status)
        {
            try
            {
                var resultado = await _mongoCollection.Find(st => st.IndiceId == status.IndiceId).FirstOrDefaultAsync();
                return resultado.Equals(null) ? false : true;
            }
            catch (Exception ex)
            {
                var detalheExcecao = new Exception($"{ex.Message} - ao consultar o objeto - {status}");

                throw detalheExcecao;

            }

        }


    }
}

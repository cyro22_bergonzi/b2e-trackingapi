﻿using ApiTrackingDominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiTrackingRepositorio.Contratos
{
    public interface IStatusRepositorio
    {
        Task<List<Status>> BuscarListaStatus();
        
        //decimal BuscarValorStatus(Guid Id, DateTime data);
        Task<Status> InsertStatusMongo(Status status);
        Task<bool> BuscaStatusExistente(Status status);

        /*
         * Entidade
         */
        Task<Status> BuscarEntidadeCliente(string orderId);
        Task<Status> BuscarEntidadeSAP(string orderId);
        Task<Status> BuscarEntidadeSalesForce(string orderId);

        /*
         * Header
         */
        Task<Status> BuscarHeaderCliente(string orderId);
        Task<Status> BuscarHeaderSAP(string orderId);
        Task<Status> BuscarHeaderSalesForce(string orderId);

        /*
         * Itens
         */
        Task<Status> BuscarStatusItensCliente(string orderId);
        Task<Status> BuscarItemCliente(string orderId, string itemId);
        Task<Status> BuscarStatusItemCliente(string orderId, string itemId);

        Task<Status> BuscarStatusItensSAP(string orderId);
        Task<Status> BuscarItemSAP(string orderId, string itemId);
        Task<Status> BuscarStatusItemSAP(string orderId, string itemId);

        Task<Status> BuscarStatusItensSalesForce(string orderId);
        Task<Status> BuscarItemSalesForce(string orderId, string itemId);
        Task<Status> BuscarStatusItemSalesForce(string orderId, string itemId);


        
    }
}
